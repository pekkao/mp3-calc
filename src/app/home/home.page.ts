import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  // Member variables
  private totalSpace: number;
  private usedSpace: number;
  private nbrOfTracks: number;
  private remainingSpace: number;
  private averageTrackSpace: number;
  private nbrOfTracksLeft: number;

  constructor() {}

  private calculate() {
    this.remainingSpace = this.totalSpace - this.usedSpace;
    this.averageTrackSpace = this.usedSpace / this.nbrOfTracks;
    this.nbrOfTracksLeft = 
      Math.floor(this.remainingSpace / this.averageTrackSpace);
  }

}